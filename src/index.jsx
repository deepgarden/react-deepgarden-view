import React from 'react';

import Text from './lib/Text';
import Date from './lib/Date';

export const types = {
	text: Text,
	date: Date
};

import './index.styl';

export default props => {
	const {type, ...restProps} = props;
	const Component = types[type] || Text;
	return Component && <Component {...restProps}/>
}