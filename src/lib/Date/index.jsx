import React from 'react';
import classNames from 'classnames';

import './index.styl';

const Date = props => {
	const {value, className, ...restProps} = props;
	return <div  {...restProps} className={classNames('_View --Date', className)}>
		{value}
	</div>
};

export default Date;