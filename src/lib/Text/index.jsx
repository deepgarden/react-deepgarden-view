import React from 'react';
import classNames from 'classnames';

import './index.styl';

const Text = props => {
	const {value, className, ...restProps} = props;
	return <div  {...restProps} className={classNames('_View --Text', className)}>
		{value}
	</div>
};

export default Text;